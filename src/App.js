import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import "./App.css";
import Cart from "./components/Cart";
import FooterFakeStore from "./components/FooterFakeStore";
import HeaderFakeStore from "./components/HeaderFakeStore";
import ProductDisplay from "./components/ProductDisplay";
import SignupForm from "./components/SignupForm";
import SingleProductDisplay from "./components/SingleProductDisplay";
import WeFailed from "./components/WeFailed";

class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      pageHeading: "Everything you need!",

    };

  };

  changePageHeading = (headingString) => {
    this.setState({
      pageHeading: headingString,
    });

  };

  render() {
    return (
      <Router>
        <div className="App d-flex flex-column align-items-center">
          <HeaderFakeStore heading={this.state.pageHeading} />

          <Switch>
            <Route
              exact path="/"
              render={(props) => {
                return <ProductDisplay {...props} pageHeadingHandler={this.changePageHeading} />
              }}
            />
            <Route
              exact path="/cart/:cartId*"
              render={(props) => {
                return <Cart {...props} pageHeadingHandler={this.changePageHeading} />

              }}
            />
            <Route
              exact path="/signup"
              render={(props) => {
                return <SignupForm {...props} pageHeadingHandler={this.changePageHeading} />

              }}
            />
            <Route
              exact path="/cart"
              render={(props) => {
                return <WeFailed {...props} />

              }}
            />
            <Route
              exact path="/:id*"
              render={(props) => {
                return <SingleProductDisplay {...props} pageHeadingHandler={this.changePageHeading} />
              }}
            />
          </Switch>

          <FooterFakeStore />
        </div>
      </Router>
    );

  };

};


export default App;

