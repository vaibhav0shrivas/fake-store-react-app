import React from "react";
import Axios from "axios";


import Loader from "./Loader";
import { Link } from "react-router-dom";
import WeFailed from "./WeFailed";
import NoProducts from "./NoProducts";

class SingleProductDisplay extends React.Component {
    constructor(props) {
        super(props);

        this.API_STATES = {
            LOADING: "loading",
            LOADED: "loaded",
            ERROR: "error",
            NOPRODUCTEXIST: "showNoProductExist",
        }

        this.state = {
            status: this.API_STATES.LOADING,
            singleProductData: null,

        };

    };

    getSingleProductFromServer = (productId) => {
        const productURL = "https://fakestoreapi.com/products/";

        this.setState({
            status: this.API_STATES.LOADING,
        },

            () => {
                Axios.get(productURL + productId)
                    .then((response) => {
                        if (response.status !== 200) {
                            throw Error("Fetch Failed");
                        }
                        return response.data;

                    })
                    .then((data) => {

                        if (data === "") {
                            this.setState({
                                status: this.API_STATES.NOPRODUCTEXIST,

                            });

                        } else {
                            this.setState({
                                status: this.API_STATES.LOADED,
                                productData: data,
                            });

                        }


                    })
                    .catch((error) => {
                        this.setState({

                            status: this.API_STATES.ERROR,

                        },
                            () => {
                                console.error(error);
                            }
                        );
                    });

            }
        );


    };
    componentDidMount() {
        this.props.pageHeadingHandler("Product Catalogue");
        document.title="Fake Store | Product Catalogue";
        this.getSingleProductFromServer(this.props.match.params.id);
    }


    render() {
        return (
            <main className="flex-grow-1">
                {
                    this.state.status === this.API_STATES.LOADING &&
                    <Loader />
                }

                {
                    this.state.status === this.API_STATES.LOADED &&
                    <div className="container-fluid mt-4">
                        <div className="image-container text-center">
                            <img src={this.state.productData.image} alt={this.state.productData.description} />
                        </div>
                        <div className="product-details">
                            <h3 className="text-center">{this.state.productData.title}</h3>

                            <h6 className="text-center">{this.state.productData.description}</h6>
                            <div className="text-center">
                                <div className="rating">Rating: <span>{this.state.productData.rating.rate}</span><i className="fa-solid fa-star"></i></div>
                                <div className="price">Price: <span> $ {this.state.productData.price}</span></div>
                            </div>
                            <div className="buttons text-center">
                                <button className="buy button">Buy</button>
                                <span>&nbsp;</span>
                                <button
                                    className="add-to-cart button">Add to cart<i className="fa-solid fa-cart-plus"></i></button>
                                <span>&nbsp;</span>
                                <Link to={`/`}><button className="go-back button">See full catalogue</button></Link>
                            </div>
                        </div>
                    </div>
                }

                {
                    this.state.status === this.API_STATES.ERROR &&
                    <WeFailed />
                }


                {
                    this.state.status === this.API_STATES.NOPRODUCTEXIST &&
                    <NoProducts />
                }
            </main>
        );
    }
}

export default SingleProductDisplay;