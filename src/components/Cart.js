import React from 'react';
import Axios from 'axios';
import Loader from "./Loader";
import WeFailed from "./WeFailed";
import ItemsInCart from './ItemsInCart';


class Cart extends React.Component {
    constructor(props) {
        super(props);

        this.API_STATES = {
            LOADING: "loading",
            LOADED: "loaded",
            ERROR: "error",
        }

        this.state = {
            cartItems: null,
            productsData: null,
            renderChild: null,
            isCartEmpty: null,
            status: this.API_STATES.LOADING,

        }

    };

    setItemsRender = () => {

        let renderChildKeys = this.state.cartItems.products.reduce((accumulator, productInCart) => {
            accumulator[productInCart.productId] = true;
            return accumulator;
        },
            {}
        );

        this.setState({
            renderChild: renderChildKeys,
        });
    };

    updateCart = () => {

        const cartUrl = `https://fakestoreapi.com/carts/${this.props.match.params.cartId}`;

        this.setState({
            status: this.API_STATES.LOADING,
        },
            () => {

                Axios.get(cartUrl)
                    .then((response) => {
                        if (response.status !== 200) {
                            throw Error("Fetch Failed");
                        }
                        return response.data;

                    })
                    .then((data) => {

                        if (data !== undefined) {
                            this.setState({
                                cartItems: data,
                                status: this.API_STATES.LOADED,
                            },
                                this.setItemsRender

                            );


                        } else {
                            this.setState({
                                status: this.API_STATES.ERROR,

                            });
                        }


                    })
                    .catch((error) => {
                        this.setState({
                            status: this.API_STATES.ERROR,

                        },
                            () => {
                                console.error(error);
                            }
                        );
                    });


            }
        );

    };

    componentDidMount() {
        this.props.pageHeadingHandler("Your Cart");
        document.title="Fake Store | Cart";
        this.updateCart();

    };

    removeItem = (productId) => {

        this.setState((prevState) => {
            let updatedRenderChild = prevState.renderChild;

            if (productId in updatedRenderChild) {
                updatedRenderChild[productId] = false;

                let updatedIsCartEmpty = !(Object.values(updatedRenderChild).reduce((accumulator, renderItem) => {
                    return accumulator || renderItem;

                },
                    false));

                return {
                    renderChild: updatedRenderChild,
                    isCartEmpty: updatedIsCartEmpty,
                };

            }
            return {};

        });


    };

    render() {
        return (
            <main className="flex-grow-1 w-100">
                {
                    this.state.status === this.API_STATES.LOADING &&
                    <Loader />
                }
                {
                    this.state.status === this.API_STATES.LOADED &&
                    this.state.cartItems !== null &&
                    <ul className="row">
                        {
                            this.state.cartItems.products.map((productInCart) => {

                                return (
                                    this.state.renderChild !== null &&
                                    this.state.renderChild[productInCart.productId] !== false &&
                                    < li className="col-10"
                                        key={productInCart.productId} >

                                        <ItemsInCart productId={productInCart.productId}
                                            quantity={productInCart.quantity}
                                            removeItemHandler={this.removeItem} />
                                    </li>

                                );

                            })
                        }
                    </ul>

                }
                {
                    this.state.isCartEmpty === true &&
                    <h6 className="text-center text-secondary">You don't have anything in your cart!</h6>
                }
                {
                    this.state.status === this.API_STATES.ERROR &&
                    <WeFailed />
                }
            </main >
        );
    };
};

export default Cart;