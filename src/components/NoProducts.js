import React from 'react';
import { Link } from "react-router-dom";


function NoProducts() {
    return (
        <div className="no-products text-center" >
            <h2>Sorry the product you are looking for does not exist!</h2>
            <p>You have the wrong link. Check out our full catalogue for other products.</p>
            <Link to={`/`}><button className="go-back button">See full catalogue</button></Link>
        </div>
    );
}

export default NoProducts;