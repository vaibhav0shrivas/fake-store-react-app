import React from "react";
import { Link } from "react-router-dom";

class HeaderFakeStore extends React.Component {

    render() { 
        return (
            <div className="container-fluid bg-dark text-light p-0 w-100">
                <header className="navbar text-center d-flex align-content-center">
                    <Link to={`/`}><h4 className="m-0"><i className="fa-solid fa-store"></i> Fake Store</h4></Link>
                    <h2 className="m-0">{this.props.heading}</h2>
                    <p className="float-right text-muted m-0">
                        <Link to={`/signup`}>Sign Up/Login
                        </Link>
                        &nbsp;&nbsp;&nbsp;
                        <Link to={`/cart/2`}>Cart &nbsp;
                            <i className="fa-solid fa-cart-shopping"></i>
                        </Link>
                    </p>
                </header>
            </div>
        );
    }
}
 
export default HeaderFakeStore;


