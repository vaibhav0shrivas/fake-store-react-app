import React from "react";
import Axios from "axios";
import { Link } from "react-router-dom";

import "../styles/ProductDisplay.css";
import Loader from "./Loader";

class ProductDisplay extends React.Component {

    constructor(props) {
        super(props);

        this.API_STATES = {
            LOADING: "loading",
            LOADED: "loaded",
            ERROR: "error",
            NOPRODUCTEXIST: "showNoProductExist",
        }

        this.state = {
            showLoader: true,
            isProductListAvailable: false,
            showNoProducts: false,
            showFailedFetch: false,
            productsData: null,
            isSingleProductAvailable: false,
            singleProductData: null,
            showFailedFetchForSingleProduct: false,
            status: this.API_STATES.LOADING,

        };

        this.productsURL = "https://fakestoreapi.com/products/";

    };


    getProductsFromServer = () => {

        this.setState({
            status: this.API_STATES.LOADING,

        },
            () => {
                Axios.get(this.productsURL)
                    .then((response) => {
                        if (response.status !== 200) {
                            throw Error("Fetch Failed");
                        }
                        return response.data;

                    })
                    .then((data) => {


                        if (data.length === 0) {
                            this.setState({
                                status: this.API_STATES.NOPRODUCTEXIST,
                            });

                        } else {
                            this.setState({
                                status: this.API_STATES.LOADED,
                                productsData: data,
                            });

                        }
                    })
                    .catch((error) => {
                        this.setState({
                            status: this.API_STATES.ERROR,

                        },
                            () => {
                                console.error(error);
                            }
                        );
                    });

            }
        );

    };


    componentDidMount() {
        this.props.pageHeadingHandler("Product Catalogue");
        document.title="Fake Store | Product Catalogue";
        this.getProductsFromServer();
    };

    render() {
        return (
            <main className="flex-grow-1">
                {
                    this.state.status === this.API_STATES.LOADING &&
                    <Loader />
                }

                {
                    this.state.status === this.API_STATES.ERROR &&

                    <div className="we-failed" >
                        <h2>Unable to load the data</h2>
                        <p>This can happen if you are not connected to the internet, or if an underlying system or component is not available.</p>
                        <a href="https://www.lifewire.com/cant-connect-to-the-internet-try-this-817794">Get Help</a>
                    </div>
                }


                {
                    this.state.status === this.API_STATES.NOPRODUCTEXIST &&
                    <div className="no-products" >
                        <h2>Sorry something went wrong from our side</h2>
                        <p>We failed to show you our products but we are working on it.</p>
                    </div>


                }

                {
                    this.state.status === this.API_STATES.LOADED &&

                    <ul className="productList">
                        {
                            this.state.productsData.map((productObject) => {

                                return (
                                    <li className="listItem"
                                        key={productObject.id}
                                    >
                                        <div className="productCard">
                                            <div className="image-container">
                                                <img src={productObject.image} alt={productObject.description} />
                                            </div>
                                            <Link to={`/${productObject.id}`}><h3>{productObject.title}</h3></Link>
                                            <div>
                                                <div className="rating">
                                                    Rating: <span>{productObject.rating.rate}</span><i className="fa-solid fa-star"></i>
                                                </div>
                                                <div className="price">
                                                    Price: <span> $ {productObject.price}</span>
                                                </div>
                                            </div>
                                            <div className="buttons">
                                                <button className="buy button">Buy</button>
                                                <span>&nbsp;</span>
                                                <button
                                                    className="add-to-cart button">Add to cart<i className="fa-solid fa-cart-plus"></i></button>
                                            </div>
                                        </div>
                                    </li>
                                );


                            })
                        }
                    </ul>

                }

            </main>
        );

    };

};


export default ProductDisplay;