import React from "react";
import Axios from "axios";
import { Link } from "react-router-dom";

import Loader from "./Loader";

class ItemsInCart extends React.Component {
    constructor(props) {
        super(props);

        this.API_STATES = {
            LOADING: "loading",
            LOADED: "loaded",
            ERROR: "error",
        }

        this.state = {
            singleProductData: null,
            quantity: props.quantity,
            totalPrice: null,
            status: this.API_STATES.LOADING,

        };

    };

    getSingleProductFromServer = (productId) => {
        const productURL = "https://fakestoreapi.com/products/";

        this.setState({
            status: this.API_STATES.LOADING,
        },

            () => {
                Axios.get(productURL + productId)
                    .then((response) => {
                        if (response.status !== 200) {
                            throw Error("Fetch Failed");
                        }
                        return response.data;

                    })
                    .then((data) => {

                        if (data === "") {
                            this.setState({
                                status: this.API_STATES.ERROR,
                                
                            });

                        } else {
                            let Amount = Number.parseInt(this.props.quantity) * Number.parseFloat(data.price);
                            this.setState({
                                status: this.API_STATES.LOADED,
                                productData: data,
                                totalPrice: Amount,
                            });

                        }


                    })
                    .catch((error) => {
                        this.setState({

                            status: this.API_STATES.ERROR

                        },
                            () => {
                                console.error(error);
                            }
                        );
                    });

            }
        );


    };

    incrementQuantity = () => {
        this.setState((prevState) => {
            return {
                quantity: (prevState.quantity + 1),
                totalPrice: ((prevState.quantity + 1) * prevState.productData.price),
            };

        });

    };

    decrementQuantity = () => {

        this.setState((prevState) => {
            let newQuantity = (prevState.quantity - 1) <= 0 ? 0 : (prevState.quantity - 1);
            return {
                quantity: newQuantity,
                totalPrice: (newQuantity * prevState.productData.price),
            };

        });

    };

    componentDidMount() {

        this.getSingleProductFromServer(this.props.productId);
    };

    render() {
        return (
            <div>
                {
                    this.state.status === this.API_STATES.LOADING &&
                    <Loader />
                }
                {
                    this.state.status === this.API_STATES.LOADED &&
                    <div className="container-fluid m-3 row">
                        <div className="image-container w-20px h-20px col-4 text-center">
                            <img src={this.state.productData.image}
                                alt={this.state.productData.description} />
                            <Link to={`/${this.state.productData.id}`}><span>{this.state.productData.title}</span></Link>
                        </div>
                        <div className="product-details col-6 text-center row">
                            <div className="col-12">Price : <span> $ {this.state.productData.price}</span></div>
                            <div className="col-12">
                                {
                                    this.state.quantity > 0 &&
                                    <button className="Decrement button p-2"
                                        onClick={this.decrementQuantity}>-</button>
                                }
                                &nbsp;
                                Quantity : <span> {this.state.quantity}</span>
                                &nbsp;
                                <button className="Increment button p-2"
                                    onClick={this.incrementQuantity}>+</button>
                            </div>
                            <div className="col-12">Amount :
                                <span> $ {Number(this.state.totalPrice).toFixed(2)}</span>
                            </div>
                        </div>
                        <div className="col-1">
                            <button className="button p-1"
                                onClick={() => {
                                    this.props.removeItemHandler(this.state.productData.id);
                                }}>Remove Item</button>
                        </div>

                    </div >

                }
                {
                    this.state.status === this.API_STATES.ERROR &&
                    <h6>Failed to load cart product!!</h6>
                }
            </div>
        )
    }
};

export default ItemsInCart;