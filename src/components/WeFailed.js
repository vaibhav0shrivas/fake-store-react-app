import React from 'react';
import { Link } from "react-router-dom";

function WeFailed() {
    return (
        <div className="we-failed text-center" >
            <h2>We failed to load the data</h2>
            <p>This can happen if you have a broken link!</p>
            <Link to={`/`}><button className="go-back button">See full catalogue</button></Link>
        </div>
    );
}

export default WeFailed;